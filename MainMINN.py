import tensorflow as tf

# from tensorflow import keras
# from tensorflow_core.python.keras.api._v2.keras import layers
import keras
from keras import layers

import numpy as np
import matplotlib.pyplot as plt
import Source.Classes.MINNClass as MINN
import scipy.io

import os
from os.path import join as pjoin
import time
import math

layerStructure = [1, 20, 20 ,30, 30, 30, 30, 30, 1]
learningRateNet = 0.00001
learningRateOp = -1.0

batchSolSize = 1
batchParSize = 32
nDivPartition = 500

np.random.seed(532)

# !TO START TENSORBOARD: tensorboard --logdir Logs/Tensorflow/MINN1

#* Loging Direction
log_dir = pjoin(os.path.dirname(os.path.realpath(__file__)), 'Logs', 'Tensorflow', 'MINN', 'LIN')
#* Net Object Creation
NetObject = MINN.MINNet(layerStructure, learningRateNet, learningRateOp, log_dir, 542)

#* Create differential operator
alpha = tf.Variable([1.0], dtype=tf.float32, name="Alpha")
beta = tf.Variable([2.0], dtype=tf.float32, name="Beta")
omega = tf.Variable([1.0], dtype=tf.float32, name="Omega")

F = lambda X, Y, Y_X : 1.2*Y - 1.2*Y*Y - Y_X
# f = lambda X : math.cos(X)*math.sin(2*X)

NetObject.SetDiffOperator(F)
# NetObject.SetDiffOperatorVariables([alpha, beta])
NetObject.SetDiffOperatorVariables([])
NetObject.trainWithDiffOperator = True

#! -------------------------
#|| Loading data
data_dir = pjoin(os.path.dirname(os.path.realpath(__file__)), 'Data', 'Final')
mat_fname = pjoin(data_dir, 'data_lin1.mat')

print(data_dir)

data = scipy.io.loadmat(mat_fname)

t_raw = data['t']
y_raw = data['y']

t_raw = t_raw.flatten()[:, None]
y_raw = y_raw.flatten()[:, None]

t_raw = t_raw.astype('float32')
y_raw = y_raw.astype('float32')

# idx = np.random.choice(t_raw.shape[0], 10, replace=False)
# 
# idx.sort()
# 
# t_train = t_raw[idx, :]
# y_train = y_raw[idx, :]

# lb = 0.0
# ub = math.pi * 2
# dx = (ub - lb)/1000.0
# 
# t_raw = np.zeros(1000)
# y_raw = np.zeros(1000)
# 
# for i in range(1000):
#     t_raw[i] = lb + i*dx
#     y_raw[i] = f(t_raw[i])

t_train = np.array([t_raw[0], t_raw[-1]])
y_train = np.array([y_raw[0], y_raw[-1]])

t_train = t_train.flatten()[:, None]
y_train = y_train.flatten()[:, None]

lowerBoud = min(t_raw)
upperBoud = max(t_raw)

dx = (upperBoud - lowerBoud)/nDivPartition

X = np.zeros(nDivPartition)

for i in range(nDivPartition):
    X[i] = lowerBoud + i*dx

X = X.flatten()[:, None]
X = X.astype('float32')

#|| Creating the datasets
train_dataset = tf.data.Dataset.from_tensor_slices((t_train, y_train))
train_dataset = train_dataset.shuffle(len(t_raw)*2)
# train_dataset = train_dataset.take(250)
train_dataset = train_dataset.batch(batchSolSize)

partition_dataset = tf.data.Dataset.from_tensor_slices(X)
partition_dataset = partition_dataset.shuffle(nDivPartition*2)
partition_dataset = partition_dataset.batch(batchParSize)

#|| Generate initial plot
data_Y_pred_old = NetObject.Model.predict(t_raw)

plt.xlabel('t')
plt.ylabel('y')
plt.plot(t_raw, y_raw, 'b')
plt.plot(t_raw, data_Y_pred_old, 'k')
plt.plot(t_train, y_train, 'r*')

plt.plot(X, np.zeros(nDivPartition), 'r|')

plt.draw()
plt.pause(0.001)

x_min, x_max = plt.gca().get_xlim()
y_min, y_max = plt.gca().get_ylim()

start_time = time.time()
#|| Train
# NetObject.Train(15000, train_dataset, int(250/batchSolSize), [t_train, y_train], 100)
NetObject.HybridTrain(50000, train_dataset, partition_dataset, [t_train, y_train, X], 100, [x_min, x_max, 0.4, 1.1])

print("Time: " + str(time.time() - start_time))

#|| Plot result
data_Y_pred = NetObject.Model.predict(t_raw)

plt.plot(t_raw, y_raw, 'k*')
plt.plot(t_raw, data_Y_pred, 'g*')

full_fig= plt.figure(num="MINNet " + NetObject.startTime)
plt.xlabel('t')
plt.ylabel('y')

# dataEnumerate = enumerate(train_dataset)
# for (batch, (netInput, netOutput)) in dataEnumerate:
#     plt.plot(netInput, netOutput, 'r*')

plt.plot(t_raw, y_raw, 'b')
plt.plot(t_train, y_train, 'r*')
plt.plot(t_raw, data_Y_pred, 'g')
plt.plot(t_train, NetObject.Model.predict(t_train), 'k*')

plt.draw()
plt.pause(0.001)

plt.savefig(NetObject.mainPath + '/Results.png')